function reverseArray(array)
	local rArray = {}
	for i = #array,1,-1 do
		table.insert(rArray, array[i])
	end
	return rArray
end

function shuffleArray(array)
	size = #array
	for i = size, 1, -1 do
		local rand = random(size)
		array[i], array[rand] = array[rand], array[i]
	end
	return array
end
