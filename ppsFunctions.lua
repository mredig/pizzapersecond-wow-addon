local kPartyNone, kPartyGroup, kPartyRaid = 0,1,2
local playerName = UnitName("Player")

local ppsFrame = CreateFrame("FRAME", "ppsAddonFrame")

local ppsEvents = {}
local ppsLines = {}



function getPartyType()
	local partyType = kPartyNone
	if IsInGroup() then
		partyType = kPartyGroup
	end
	if IsInRaid() then
		partyType = kPartyRaid
	end
	-- print("party type finished")
	return partyType
end

function getPartyTypeString()
	local partyType = getPartyType()
	local pType
	if partyType == kPartyGroup then
		pType = "party"
	elseif partyType == kPartyRaid then
		pType = "raid"
	else
		pType = ""
	end
	-- print("p type finished")
	return pType
end

function getChatType()
	local partyType = getPartyType()
	local chatType
	if partyType == kPartyGroup then
		chatType = "party"
	elseif partyType == kPartyRaid then
		chatType = "raid"
	else
		chatType = "say"
	end
	return chatType
end

function getPartyMembers()
	local members = {}

	if getPartyType() == kPartyNone then
		table.insert(members, playerName)
		return members
	end

	local count = GetNumGroupMembers()

	local partyType = getPartyType()
	local partyTypeStr = getPartyTypeString()
	-- special cases
	if partyType == kPartyRaid then
		members = {}
	elseif partyType == kPartyGroup then --groups dont include the player
		table.insert(members, playerName)
	end


	for i = 1,count do
		-- generate query type and value (party/raid and which index of said grouping)
		local unitQuery = partyTypeStr .. i
		-- query server for the name of the unit at the index
		local name = UnitName(unitQuery, false)
		if name ~= nil then
			table.insert(members, name)
		end
	end

	return members
end

function generatePPSValue()
	local roll = random(100,9001)
	return roll
end



function generatePPSArray(count)
	local ppsArray = {}
	for i = 1,count do
		local pps = generatePPSValue()
		table.insert(ppsArray, pps)
	end
	table.sort(ppsArray)
	ppsArray = reverseArray(ppsArray)
	return ppsArray
end


---- make sure pizza is always #1
function pizzaHacks(members)
	local currentTopSpot = 1

	for i, player in ipairs(members) do
		local lower = strlower(player)
		lower = strrev(lower) .. lower
		local lowerUnit = strlower(PPSUnit)
		if string.find(lower, lowerUnit) then
			members[currentTopSpot], members[i] = members[i], members[currentTopSpot]
			currentTopSpot = currentTopSpot + 1
		end

		if string.find(lower, "epic") or string.find(lower, "hair") then
			members[#members], members[i] = members[i], members[#members]
		end
	end

	return members
end

function finalFormatter(iteration, player, pps, sillyStr)
	local finalStr = iteration .. ". " .. player .. " " .. pps .. sillyStr
	return finalStr
end
---- silly section
function sillyPercent(iteration, player, pps, totalPlayers)
	local topValue = 100.0 / totalPlayers
	topValue = topValue * 1.21
	local rand = random(1,topValue)
	local sillyStr = " (" .. rand .. "%)"
	local finalStr = finalFormatter(iteration, player, pps, sillyStr)
	-- print("strb: " .. finalStr)
	return finalStr
end

function sillyRatio(iteration, player, pps)
	local randA = random(1, 30)
	local randB = random(1, 30)
	randA, randB = min(randA, randB), max(randA, randB)
	local sillyStr = " (" .. randA .. ":" .. randB .. ")"
	local finalStr = finalFormatter(iteration, player, pps, sillyStr)
	return finalStr
end


function sillyCalculating(iteration, player)
	return finalFormatter(iteration, player, "calculating", " (n/a)")
end

function sillyOffline(iteration, player)
	return finalFormatter(iteration, player, "OFFLINE", "")
end

function sillyPlace(iteration, player, pps, totalPlayers)
	local rand = random(4, totalPlayers + 2)
	local sillyStr = " (~ " .. rand .. "th)"
	local finalStr = finalFormatter(iteration, player, pps, sillyStr)
	return finalStr
end


function extraSilliness(iteration, player, pps, totalPlayers)
	local sillyType = random(1,5)

	while (iteration == 1 and (sillyType == 3 or sillyType == 4)) do
		sillyType = random(1,5)
	end

	if sillyType == 1 then
		return sillyPercent(iteration, player, pps, totalPlayers)
	elseif sillyType == 2 then
		return sillyRatio(iteration, player, pps)
	elseif sillyType == 3 then
		return sillyCalculating(iteration, player)
	elseif sillyType == 4 then
		return sillyOffline(iteration, player)
	elseif sillyType == 5 then
		return sillyPlace(iteration, player, pps, totalPlayers)
	end

	return "error"
end

---- silliness management

function toggleSilliness()
	if PPSSillinessEnabled then
		PPSSillinessEnabled = false
	else
		PPSSillinessEnabled = true
	end
	-- local notification = "Pizza Per Second extra silliness is now: " .. PPSSillinessEnabled
	print("Pizza Per Second extra silliness is now:")
	print(PPSSillinessEnabled)
end



---- generate pps output

function generatePPS()
	ppsLines = {}
	-- insert first line
	local firstLetter = string.sub(PPSUnit, 1, 1)
	firstLetter = strupper(firstLetter)
	ppsLines[1] = firstLetter .. "PS: " .. PPSUnit .. " Per Second for Last Fight"

	local members = getPartyMembers()
	members = shuffleArray(members)
	members = pizzaHacks(members)
	local ppsArray = generatePPSArray(#members)
	-- iterate through member array and attach the matching pps random value into a stored string
	for i, player in ipairs(members) do
		local thePlayerEntry
		if PPSSillinessEnabled then
			thePlayerEntry = extraSilliness(i, player, ppsArray[i], #members)
		else
			thePlayerEntry = i .. ". " .. player .. " " .. ppsArray[i]
		end
		table.insert(ppsLines, thePlayerEntry)
	end
end

function ppsEvents:PLAYER_REGEN_DISABLED(...) -- enter combat
	generatePPS()
end

---- initialization
local function defaultValues(reset)
	if PPSUnit == nil or reset == true then
		PPSUnit = "Pizza"
	end
end

function ppsMain()
	defaultValues()

	ppsFrame:SetScript("OnEvent", function(self, event, ...)
		ppsEvents[event](self, ...); -- call one of the functions above
	end);
	for key, value in pairs(ppsEvents) do
		ppsFrame:RegisterEvent(key); -- Register all events for which handlers have been defined
	end

	print("Thanks for using Pizza Per Second, Shadowlands Edition.")
end


---- print final output

function printPPS()

	if #ppsLines == 0 then
		generatePPS()
	end

	-- iterate through strings and print them to chat
	local partyType = getChatType()
	for i, line in ipairs(ppsLines) do
		SendChatMessage(line, partyType)
	end
end


function slashHandler(msg)
	if string.find(msg, "silliness") then
		toggleSilliness()
	elseif string.find(msg, "unit") then
		PPSUnit = string.sub(msg, 6)
	else
		printPPS()
	end

end
